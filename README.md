# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://lusauer@bitbucket.org/lusauer/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/lusauer/stroboskop/commits/73a24620f3795b46c330d792e77afc45d05d111d

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/lusauer/stroboskop/commits/30c4ee030558d73bc41c507384e7e53daaf88cbf?at=izgled

Naloga 6.3.2:
https://bitbucket.org/lusauer/stroboskop/commits/de64a5a319a76559479313ea47a568d25176f9ad?at=izgled

Naloga 6.3.3:
https://bitbucket.org/lusauer/stroboskop/commits/4fa062a88f02a412e3b7ed681224ca84234edd37?at=izgled

Naloga 6.3.4:
https://bitbucket.org/lusauer/stroboskop/commits/acba71b1fac6361ba0f9d4d2bc2ea06e340b0a29?at=izgled

Naloga 6.3.5:

```
git checkout master; git merge izgled;
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/lusauer/stroboskop/commits/7977b6d427295ca652bc9e1b8040b56a6b7e3d02?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/lusauer/stroboskop/commits/1ff84699daa3400f9b40603cb79f55a8d97f7382

Naloga 6.4.3:
https://bitbucket.org/lusauer/stroboskop/commits/bb9458d99193497d49c292f2b691a77bead67c3f

Naloga 6.4.4:
((UVELJAVITEV))